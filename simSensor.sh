#!/bin/bash

version="1.1"

readonly ROOT_TOPIC="ESEiot/1920/"$HOSTNAME

if [ $# -ne 4 ];
then
  echo
  echo "-- Script: $0 v$version will connect to a broker and simulate"
  echo "--         periodic sensor signal."
  echo "-- Usage: $0 <url broker> <signal type> <Ts> <topic>"
  echo "   <url broker>:  broker.hivemq.com"
  echo "   <signal type>: swt (sawtooth) tra (triangle)"
  echo "   <Ts>:          sample time [sec]"
  echo "   <topic>:       MQTT topic without root text (= $ROOT_TOPIC))"
  exit 1
fi

sensorValue=0
sensorValueStep=1
readonly SENSOR_MAX=100
readonly SENSOR_MIN=-100

broker=$1
signaltype=$2
sampleTime=$3
topic=$ROOT_TOPIC"/"$4

sawTooth()
{
   sensorValue=$((sensorValue+10))
   if [ $sensorValue -gt $SENSOR_MAX ]
   then
     sensorValue=$SENSOR_MIN
   fi
}

triangle()
{
   if [ $sensorValue -ge $SENSOR_MAX ]
   then
     sensorValueStep=-1
   fi
   if [ $sensorValue -le $SENSOR_MIN ]
   then
     sensorValueStep=1
   fi
   sensorValue=$((sensorValue+sensorValueStep*10))
}

echo "--- Simulation started Ts = $sampleTime topic = '$topic'"
while true
do
   echo "--- mosquitto_pub -h $broker -t $topic -m $sensorValue"
   mosquitto_pub -h "$broker" -t "$topic" -m "$sensorValue"
   case "$signaltype" in
      swt) sawTooth
         ;;
      tra) triangle
         ;;
      *)
         echo "** ERROR signal type unknown"
         exit 2
         ;;
   esac
   sleep $sampleTime
done
